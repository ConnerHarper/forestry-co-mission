﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    // Visible in unity to allow us to change how fast we move
    public float forceStrength;

    // Variable to hold the Audio Clip that should play when we walk 
    public AudioClip footstepSound;

    // Update is called once per frame
    void Update()
    {
        // Get our rigidbody that we'll need to find the physics information 
        Rigidbody2D ourRigidbody = GetComponent<Rigidbody2D>();

        //Player movement part of the update function 

        //Animation part of the update function 
        // Find out from the rigidbody what our current horizontal and vertical speeds are
        float currentSpeedH = ourRigidbody.velocity.x;
        float currentSpeedV = ourRigidbody.velocity.y;

        // Get the animator component that we will be using for setting our animation 
        Animator ourAnimator = GetComponent<Animator>();

        // Tell our animator what the speeds are
        ourAnimator.SetFloat("speedH", currentSpeedH);
        ourAnimator.SetFloat("speedV", currentSpeedV);
        
    }

    //Move the player up when the function is called
    public void MoveUp()
    {

        // The rigid body that we will be using for movement
        Rigidbody2D ourRigidbody = GetComponent<Rigidbody2D>();

        Vector2 direction = Vector2.up;
        float forceStrength = 10;

        ourRigidbody.AddForce(direction.normalized * forceStrength);

        // Get the audio source so that we can play footstep sounds
        AudioSource ourAudioSource = GetComponent<AudioSource>();

        // Check if our clip is already playing
        if (ourAudioSource.clip == footstepSound && ourAudioSource.isPlaying)
        {
            // Do nothing - our audio source is already playing the sound effect we want!

        }
        else
        {
            ourAudioSource.clip = footstepSound;
            ourAudioSource.Play();
        }
    }

    //Move the player down when the function is called
    public void MoveDown()
    {
        // The rigid body that we will be using for movement
        Rigidbody2D ourRigidbody = GetComponent<Rigidbody2D>();

        Vector2 direction = Vector2.down;
        float forceStrength = 10;

        ourRigidbody.AddForce(direction.normalized * forceStrength);

        // Get the audio source so that we can play footstep sounds
        AudioSource ourAudioSource = GetComponent<AudioSource>();

        // Check if our clip is already playing
        if (ourAudioSource.clip == footstepSound && ourAudioSource.isPlaying)
        {
            // Do nothing - our audio source is already playing the sound effect we want!

        }
        else
        {
            ourAudioSource.clip = footstepSound;
            ourAudioSource.Play(); 
        }
    }

    //Move the player left when the function is called
    public void MoveLeft()
    {
        // The rigid body that we will be using for movement
        Rigidbody2D ourRigidbody = GetComponent<Rigidbody2D>();

        Vector2 direction = Vector2.left;
        float forceStrength = 10;

        ourRigidbody.AddForce(direction.normalized * forceStrength);

        // Get the audio source so that we can play footstep sounds
        AudioSource ourAudioSource = GetComponent<AudioSource>();

        // Check if our clip is already playing
        if (ourAudioSource.clip == footstepSound && ourAudioSource.isPlaying)
        {
            // Do nothing - our audio source is already playing the sound effect we want!

        }
        else
        {
            ourAudioSource.clip = footstepSound;
            ourAudioSource.Play();
        }
    }

    //Move the player right when the function is called
    public void MoveRight()
    {
        // The rigid body that we will be using for movement
        Rigidbody2D ourRigidbody = GetComponent<Rigidbody2D>();

        Vector2 direction = Vector2.right;
        float forceStrength = 10;

        ourRigidbody.AddForce(direction.normalized * forceStrength);

        // Get the audio source so that we can play footstep sounds
        AudioSource ourAudioSource = GetComponent<AudioSource>();

        // Check if our clip is already playing
        if (ourAudioSource.clip == footstepSound && ourAudioSource.isPlaying)
        {
            // Do nothing - our audio source is already playing the sound effect we want!

        }
        else
        {
            ourAudioSource.clip = footstepSound;
            ourAudioSource.Play();
        }
    }

    public void TestButton()
    {
        // Any code inside this function will run 
        // when the button is clicked
        Debug.Log("Button has been clicked");

    }
}
