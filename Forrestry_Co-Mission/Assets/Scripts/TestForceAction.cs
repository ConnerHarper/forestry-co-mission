﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.EventSystems; 

public class TestForceAction : MonoBehaviour
{
    Rigidbody2D ourRigidBody;

    


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Hello World");
    }

    // Update is called once per frame
    void Update()
    {
        ourRigidBody = GetComponent<Rigidbody2D>();

        Vector2 direction = Vector2.right;
        float strength = 10;

        ourRigidBody.AddForce(direction.normalized * strength);
    }
}
